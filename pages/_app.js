import '../styles/global.css'

//for loading global css -> component css will be under <component>.module.css
export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}
