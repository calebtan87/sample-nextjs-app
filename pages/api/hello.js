//API route (different from normal path routes)
//will not be part of client bundle
//use this to write server-side code which saves directly to DB for example (not for rendering pages)
export default (req, res) => {
  res.status(200).json({ text: 'Hello' })
}
